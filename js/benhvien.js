var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện đa khoa Đồng Tháp",
   "address": "144 Mai Văn Khải, Mỹ Tân, Thành Phố Cao Lãnh, Đồng Tháp",
   "Longtitude": 10.472132,
   "Latitude": 105.626213
 },
 {
   "STT": 2,
   "Name": "Bệnh viện đa khoa Sa Đéc",
   "address": "153 Khóm Hoà Khánh, Nguyễn Sinh Sắc, P 2, Thị xã  Sa Đéc, Đồng Tháp",
   "Longtitude": 10.286916,
   "Latitude": 105.772786
 },
 {
   "STT": 3,
   "Name": "Bệnh viện đa khoa khu vực Hồng Ngự",
   "address": "Ấp Bình A, Hồng Ngự, Đồng Tháp",
   "Longtitude": 10.803921,
   "Latitude": 105.351507
 },
 {
   "STT": 4,
   "Name": "Bệnh viện đa khoa khu vực Tháp Mười",
   "address": "Thị trấn Mỹ An, Tháp Mười, Đồng Tháp, Việt Nam",
   "Longtitude": 10.521136,
   "Latitude": 105.841277
 },
 {
   "STT": 5,
   "Name": "Bệnh viện Y học Cổ truyền",
   "address": "Phường 5, Thành phố Cao Lãnh, Đồng Tháp",
   "Longtitude": 10.464824,
   "Latitude": 105.629441
 },
 {
   "STT": 6,
   "Name": "Bệnh viện Phục hồi chức năng",
   "address": "167 Tôn Đức Thắng, Phường 1, Thành phố Cao Lãnh, Đồng Tháp, Việt Nam",
   "Longtitude": 10.4670693,
   "Latitude": 105.6346658
 },
 {
   "STT": 7,
   "Name": "Bệnh viện Tâm thần",
   "address": "Tỉnh Lộ 847, Nhị Mỹ, Cao Lãnh, Đồng Tháp, Việt Nam",
   "Longtitude": 10.4677828,
   "Latitude": 105.7057231
 },
 {
   "STT": 8,
   "Name": "Bệnh viện Phổi",
   "address": "Tỉnh Lộ 847, Nhị Mỹ, Cao Lãnh, Đồng Tháp, Việt Nam",
   "Longtitude": 10.4686163,
   "Latitude": 105.7066673
 },
 {
   "STT": 9,
   "Name": "Bệnh viện Da liễu",
   "address": "396 Lê Đại Hành, Phường Mỹ Phú, Thành phố Cao Lãnh, Đồng Tháp, Việt Nam",
   "Longtitude": 10.4640062,
   "Latitude": 105.6363388
 },
 {
   "STT": 11,
   "Name": "Trung tâm Y tế huyện Tân Hồng",
   "address": "Số 09 Đường Trần Phú, Khóm III, Tân Hồng, Đồng Tháp, Việt Nam",
   "Longtitude": 10.8719693,
   "Latitude": 105.464574
 },
 {
   "STT": 12,
   "Name": "Trung tâm Y tế huyện Hồng Ngự",
   "address": "Thường Thới Tiền, Hồng Ngự, Đồng Tháp, Việt Nam",
   "Longtitude": 10.8130777,
   "Latitude": 105.2441539
 },
 {
   "STT": 13,
   "Name": "Trung tâm Y tế huyện Tam Nông",
   "address": "Thị trấn Tràm Chim, Tam Nông, Đồng Tháp, Việt Nam",
   "Longtitude": 10.6760501,
   "Latitude": 105.5612185
 },
 {
   "STT": 14,
   "Name": "Trung tâm Y tế huyện Thanh Bình",
   "address": "Thị trấn Thanh Bình, Thanh Bình, Đồng Tháp, Việt Nam",
   "Longtitude": 10.5524838,
   "Latitude": 105.4935604
 },
 {
   "STT": 15,
   "Name": "Trung tâm Y tế huyện Cao Lãnh",
   "address": "Thị trấn Mỹ Thọ Huyện Cao Lãnh, Thị trấn Mỹ Thọ, Cao Lãnh, Đồng Tháp, Việt Nam",
   "Longtitude": 10.4415923,
   "Latitude": 105.6794692
 },
 {
   "STT": 16,
   "Name": "Trung tâm Y tế huyện Tháp Mười",
   "address": "Nguyễn Văn Cừ, Thị trấn Mỹ An, Tháp Mười, Đồng Tháp, Việt Nam",
   "Longtitude": 10.5213947,
   "Latitude": 105.8397209
 },
 {
   "STT": 17,
   "Name": "Trung tâm Y tế huyện Lấp Vò",
   "address": "ĐT852B, Ấp Bình Hiệp A, Lấp Vò, Đồng Tháp, Việt Nam",
   "Longtitude": 10.3524604,
   "Latitude": 105.5360872
 },
 {
   "STT": 18,
   "Name": "Trung tâm Y tế huyện Lai Vung",
   "address": "Thị trấn Lai Vung, Lai Vung, Đồng Tháp, Việt Nam",
   "Longtitude": 10.2918495,
   "Latitude": 105.6448048
 },
 {
   "STT": 19,
   "Name": "Trung tâm Y tế huyện Châu Thành",
   "address": "Phú Mỹ Hiệp Thị trấn Cái Tàu Hạ Huyện Châu Thành, Phú Hựu, Châu Thành, Đồng Tháp, Việt Nam",
   "Longtitude": 10.3456502,
   "Latitude": 105.4881289
 },
 {
   "STT": 20,
   "Name": "Trung tâm Y tế thị xã Hồng Ngự",
   "address": "Đường Chu Văn An, Phường An Thạnh, Hồng Ngự, Đồng Tháp, Việt Nam",
   "Longtitude": 10.8073191,
   "Latitude": 105.3369623
 },
 {
   "STT": 21,
   "Name": "Trung tâm Y tế thành phố Sa Đéc",
   "address": "Phường 1, Sa Đéc, Đồng Tháp, Việt Nam",
   "Longtitude": 10.3050544,
   "Latitude": 105.7552615
 },
 {
   "STT": 22,
   "Name": "Trung tâm Y tế thành phố Cao Lãnh",
   "address": "Đường Trần Thị Nhượng, Phường 4, Thành phố Cao Lãnh, Đồng Tháp, Việt Nam",
   "Longtitude": 10.4527351,
   "Latitude": 105.5958545
 }
];