var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện Đa Khoa trung ương Cần Thơ",
   "address": "Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0287631,
   "Latitude": 105.755399
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Tim mạch Thành Phố Cần Thơ",
   "address": "204 Trần Hưng Đạo, Phường An Nghiệp, Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0350462,
   "Latitude": 105.773056
 },
 {
   "STT": 3,
   "Name": "Bệnh viện 121",
   "address": "Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0295903,
   "Latitude": 105.7801685
 },
 {
   "STT": 4,
   "Name": "Bệnh viện Nhi Đồng Thành Phố Cần Thơ",
   "address": "345 Nguyễn Văn cừ, Phường An Bình, Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0178758,
   "Latitude": 105.7366642
 },
 {
   "STT": 5,
   "Name": "Bệnh viện Đa Khoa Thành Phố Cần Thơ",
   "address": "Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0313896,
   "Latitude": 105.7800721
 },
 {
   "STT": 6,
   "Name": "Bệnh viện Đa Khoa Quận Ô Môn",
   "address": "Quận Ô Môn, Thành Phố Cần Thơ",
   "Longtitude": 10.1141628,
   "Latitude": 105.6206537
 },
 {
   "STT": 7,
   "Name": "Bệnh viện Đa Khoa  Quận Thốt Nốt",
   "address": "Huyện Thốt Nốt, Thành Phố Cần Thơ",
   "Longtitude": 10.2657965,
   "Latitude": 105.5384763
 },
 {
   "STT": 8,
   "Name": "Bệnh viện Đa Khoa Huyện Vĩnh Thạnh",
   "address": "Huyện Vĩnh Thạnh, Thành Phố Cần Thơ",
   "Longtitude": 10.2206028,
   "Latitude": 105.385586
 },
 {
   "STT": 9,
   "Name": "Bệnh viện Y học cổ truyền",
   "address": "Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.6604429,
   "Latitude": 105.4627607
 },
 {
   "STT": 10,
   "Name": "Bệnh viện Mắt - Răng Hàm Mặt",
   "address": "Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0388503,
   "Latitude": 105.7841424
 },
 {
   "STT": 11,
   "Name": "Bệnh viện Tai Mũi Họng",
   "address": "Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0388973,
   "Latitude": 105.7688214
 },
 {
   "STT": 12,
   "Name": "Bệnh viện Lao và bệnh Phổi Thành Phố Cần Thơ",
   "address": "Khu vực Bình Hòa A, Phường Phước Thới, Quận Ô Môn, Thành Phố Cần Thơ",
   "Longtitude": 10.1036134,
   "Latitude": 105.6663698
 },
 {
   "STT": 13,
   "Name": "Bệnh viện Da liễu",
   "address": "Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0129383,
   "Latitude": 105.7525366
 },
 {
   "STT": 14,
   "Name": "Bệnh viện Tâm thần Cần Thơ",
   "address": "Khu vực Bình Hòa A, Phường Phước Thới, Quận Ô Môn, Thành Phố Cần Thơ",
   "Longtitude": 10.066514,
   "Latitude": 105.6778145
 },
 {
   "STT": 15,
   "Name": "Bệnh viện Ung Bướu Thành Phố Cần Thơ",
   "address": "Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.030904,
   "Latitude": 105.7809989
 },
 {
   "STT": 16,
   "Name": "Bệnh viện Quân dân y Thành Phố Cần Thơ",
   "address": "Huyện Cờ Đỏ, Thành Phố Cần Thơ",
   "Longtitude": 10.0861304,
   "Latitude": 105.4330092
 },
 {
   "STT": 17,
   "Name": "Bệnh viện Trường Đại học Y Dược",
   "address": "Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0348033,
   "Latitude": 105.7532896
 },
 {
   "STT": 18,
   "Name": "Bệnh viện Huyết học - Truyền máu TPCT",
   "address": "Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0291936,
   "Latitude": 105.7516064
 },
 {
   "STT": 19,
   "Name": "Bệnh viện Phụ sản Thành Phố Cần Thơ",
   "address": "Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0496242,
   "Latitude": 105.7737819
 },
 {
   "STT": 20,
   "Name": "Bệnh viện Đa Khoa Công an TP. Cần Thơ",
   "address": "Số 9B đường Trần Phú, Phường Cái Khế, Quận Ninh Kiều, Thành Phố Cần Thơ",
   "Longtitude": 10.0313896,
   "Latitude": 105.7800721
 }
];